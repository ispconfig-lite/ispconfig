<?php

/*
Copyright (c) 2007, Till Brehm, projektfarm Gmbh
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ISPConfig nor the names of its contributors
      may be used to endorse or promote products derived from this software without
      specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/******************************************
* Begin Form configuration
******************************************/

$list_def_file = "list/web_domain.list.php";
//* Two possibilities of tform, one for each webserver
$tform_def_file_apache = 'form/web_domain-apache.tform.php';
$tform_def_file_nginx= 'form/web_domain-nginx.tform.php';
//* By default, webserver is Nginx.
$tform_def_file = $tform_def_file_nginx;

/******************************************
* End Form configuration
******************************************/

require_once('../../lib/config.inc.php');
require_once('../../lib/app.inc.php');

//* Check permissions for module
$app->auth->check_module_permissions('sites');

$app->uses('tpl,tform,tform_actions');
$app->load('tform_actions');

class page_action extends tform_actions {
	
	function onDelete() {
		global $app, $conf, $tform_def_file;		
		// Importing ID
                $this->id = (isset($_REQUEST["id"]))?intval($_REQUEST["id"]):0;
		if(count($_POST) > 1) {
                        $this->dataRecord = $_POST;
		}

		if($_SESSION["s"]["user"]["typ"] != 'admin') {
			/** 
			 * Only the admin has the right to choose which webserver, the other ones will use the default one
			*/
			$webserver = 'nginx';
		} else {
			if(!empty($this->dataRecord['webserver'])) {
				/*
				 * Either we are editing an existing record and we've just changed the webserver,
				 * or we're creating a new one and we've just chosen the webserver,
				 * in these two cases, let's load the one we've just chosen
				 */
				$webserver = $this->dataRecord['webserver'][0];echo 'YAHOUPOUET';
			} else if ($this->id > 0) {
				/* We're editing an existing record and we don't have any webserver chosen,
				 * we have to see if we have to display the tform related to nginx or nginx
				 */echo 'YOPLAPOUM';
				$record = $app->db->queryOneRecord("SELECT webserver FROM web_domain WHERE domain_id = ".intval($this->id));
				$webserver = $record['webserver'];
			}
			/* The only other case possible is that we're just creating a new web domain and we haven't finished yet the first step (choice of the webserver)
			 * in this case, there's nothing to do, let the one by default,
			 * anyway the first tab is the same for apache tform and nginx
			 * as it is the webserver choice (and that's why we have no "else"
			 */

			//* Select the right tform to use in function of the selected webserver
			switch($webserver) {
				default:
				case 'nginx':echo 'MEGAPOUET::'.$webserver.'::'.$this->id.'::'.$this->dataRecord['id'].'::'.$_POST['id'].';';
					global $tform_def_file_nginx;
					$tform_def_file = $tform_def_file_nginx;
					break;
				case 'apache2':
					global $tform_def_file_apache;
					$tform_def_file = $tform_def_file_apache;
					break;
			}
		}		

		//* Now that we're done, let the place to the original onLoad function :
		parent::onDelete();	
	}

	function onBeforeDelete() {
		global $app; $conf;
		var_dump($app->tform);
		if($app->tform->checkPerm($this->id,'d') == false) { echo 'POUPOUET:'.$this->id.':';$app->error($app->lng('error_no_delete_permission'));}
		
		// Delete all records that belog to this zone.
		$records = $app->db->queryAllRecords("SELECT domain_id FROM web_domain WHERE parent_domain_id = '".intval($this->id)."' AND type != 'vhost'");
		foreach($records as $rec) {
			$app->db->datalogDelete('web_domain','domain_id',$rec['domain_id']);
		}
		
		// Delete all records that belog to this zone.
		$records = $app->db->queryAllRecords("SELECT ftp_user_id FROM ftp_user WHERE parent_domain_id = '".intval($this->id)."'");
		foreach($records as $rec) {
			$app->db->datalogDelete('ftp_user','ftp_user_id',$rec['ftp_user_id']);
		}
		
		// Delete all records that belog to this zone.
		$records = $app->db->queryAllRecords("SELECT shell_user_id FROM shell_user WHERE parent_domain_id = '".intval($this->id)."'");
		foreach($records as $rec) {
			$app->db->datalogDelete('shell_user','shell_user_id',$rec['shell_user_id']);
		}
        
        // Delete all records that belog to this zone.
        $records = $app->db->queryAllRecords("SELECT id FROM cron WHERE parent_domain_id = '".intval($this->id)."'");
        foreach($records as $rec) {
            $app->db->datalogDelete('cron','id',$rec['id']);
        }
	}
}

$page = new page_action;
$page->onDelete();

?>
