<?php

require('lib/config.inc.php');
require('lib/app.inc.php');

set_time_limit(0);
ini_set('error_reporting',E_ALL & ~E_NOTICE);

//$today=date('Ymd');
$yesterday=date('Ymd', time()-86399);

chdir($conf['ispconfig_log_dir']);

$dirs = glob('nginx/*/');
foreach($dirs as $dir) {
	$domain = str_replace('nginx/', '', $dir);
	// Put the content of the access.log of today in the file supposed to be used for yesterday :
	file_put_contents('httpd/'.$domain.$yesterday.'-access.log', file_get_contents($dir.'access.log.1'));
	// Empty the current access.log to have only statistics about today at next daily logrotation :
	unlink($dir.'access.log.1');
}

?>